function afficherAlphabet() {
    //initialisation de la valeur de retour
    let affTxt = "";
    //Code Unicode de 65 à 65 + 26
    for (let code = 65; code < 65 + 26; code++) {
      //trouver la lettre correspondant au code Unicode
      let lettre = String.fromCharCode(code); //"A" à "Z"
      //Mettre bout à bout la lettre à la valeur de retour
      affTxt += lettre;
    }
    //renvoyer la chaine de caractère
    return affTxt;
  }
  
  //appel de la fonction
  let alphabet = afficherAlphabet();
  //affichage de l'alphabet
  alert(alphabet);